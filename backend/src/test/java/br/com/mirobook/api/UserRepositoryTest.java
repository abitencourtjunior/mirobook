package br.com.mirobook.api;

import javax.validation.ConstraintViolationException;

import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.mirobook.model.User;
import br.com.mirobook.repository.UserRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {
	
	@Autowired
	private UserRepository userRepository;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void createUserEntity() {
		String name = "Test";
		String mail = "teste@segware.com";
		String password = "pass1234";
		
		User newUser = new User(name, mail,password);
		this.userRepository.save(newUser);
		
		System.out.println(newUser.getName());
		
		Assertions.assertThat(newUser.getId()).isNotNull();
		Assertions.assertThat(newUser.getName()).isEqualTo(name);
		Assertions.assertThat(newUser.getMail()).isEqualTo(mail);
		
	}
	
	@Test
	public void updateUserEntity() {
		String name = "Test";
		String mail = "teste@segware.com";
		String password = "pass1234";
		
		String nameUpdate = "Test";
		String passwordUpdate = "pass1234";

		User newUser = new User(name, mail,password);
		this.userRepository.save(newUser);
		
		newUser.setName(nameUpdate);
		newUser.setPasswordUser(passwordUpdate);
		
		this.userRepository.save(newUser);
		newUser = this.userRepository.getOne(newUser.getId());

		Assertions.assertThat(newUser.getId()).isNotNull();
		Assertions.assertThat(newUser.getName()).isEqualTo(name);
		Assertions.assertThat(newUser.getMail()).isEqualTo(mail);
	}
	
	@Test
	public void deleteUserEntity() {
		String username = "test one";
		String password = "pass1234";
		
		User newUser = new User(username, password);
		
		this.userRepository.save(newUser);
		this.userRepository.delete(newUser);
		
		Assertions.assertThat(this.userRepository.findById(newUser.getId())).isEmpty();
	}
	
	@Test
	public void findByUsernameEntity() {
		String name = "Test";
		String mail = "teste@segware.com";
		String password = "pass1234";
		
		User newUser = new User(name, mail,password);
		this.userRepository.save(newUser);
		
		User userFind = this.userRepository.findByName(name);
		
		Assertions.assertThat(userFind.getId()).isNotNull();
		Assertions.assertThat(userFind.getName()).isEqualTo(name);
		Assertions.assertThat(userFind.getMail()).isEqualTo(mail);
		
	}
		
}
