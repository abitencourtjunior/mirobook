package br.com.mirobook.api;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.mirobook.controller.form.UserForm;
import br.com.mirobook.model.User;
import br.com.mirobook.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class UserEndpointTest {
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@MockBean
	private UserRepository userRepository;
	
	@Test
	public void createUserWhenTokenCorrectShouldReturnStatus201() {
		UserForm userForm = new UserForm("Altamir", "altamir@segware.com", "segware2019");
		
		User user = userForm.converter();
		BDDMockito.when(userRepository.save(user)).thenReturn(user);
		
		ResponseEntity<User> response = restTemplate.exchange("/v1/user", HttpMethod.POST, new HttpEntity<>(userForm), User.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(201);
	}
	
	@Test
	public void createUserWithNoUsernameWhenTokenCorrectShouldReturnStatus400() {
		UserForm userForm = new UserForm(null, "altamir@segware.com", "segware2019");
		
		User newUser = userForm.converter();
		BDDMockito.when(this.userRepository.save(newUser)).thenReturn(newUser);
		
		ResponseEntity<String> response = restTemplate.exchange("/v1/user", HttpMethod.POST, new HttpEntity<>(userForm), String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(400);
		Assertions.assertThat(response.getBody()).contains("defaultMessage", "The name cannot be undefined");
	}

}
