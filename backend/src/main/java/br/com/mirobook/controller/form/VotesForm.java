package br.com.mirobook.controller.form;

import javax.validation.constraints.NotNull;

public class VotesForm {
	
	@NotNull
	private Long idUser;
	@NotNull
	private Long idPost;
	
	public Long getIdUser() {
		return idUser;
	}
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
	public Long getIdPost() {
		return idPost;
	}
	public void setIdPost(Long idPost) {
		this.idPost = idPost;
	}
	
}
