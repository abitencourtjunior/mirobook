package br.com.mirobook.controller.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import br.com.mirobook.model.Post;

public class DetailPostDto {

	private String description;
	private String nameUser;
	private LocalDateTime dateCreated;
	private List<DetailVotesDto> listVotes;

	public DetailPostDto(Post post) {
		this.description = post.getDescription();
		this.nameUser = post.getUser().getName();
		this.dateCreated = post.getDateCreated();
		this.listVotes = new ArrayList<>();
		this.listVotes.addAll(post.getListVotes().stream().map(DetailVotesDto::new).collect(Collectors.toList()));
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNameUser() {
		return nameUser;
	}

	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

	public LocalDateTime getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}

	public List<DetailVotesDto> getListVotes() {
		return listVotes;
	}

	public void setListVotes(List<DetailVotesDto> listVotes) {
		this.listVotes = listVotes;
	}



}
