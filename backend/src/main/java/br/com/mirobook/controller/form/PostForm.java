package br.com.mirobook.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.com.mirobook.model.Post;
import br.com.mirobook.model.User;
import br.com.mirobook.repository.UserRepository;


public class PostForm {

	@NotNull
	@NotEmpty
	@Length(max = 280)
	private String description;

	@NotNull
	@NotEmpty
	private String name;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Post converter(UserRepository userRepository) {
		User user = userRepository.findByName(name);
		return new Post(description, user);
	}

}
