package br.com.mirobook.controller.dto;

import java.time.LocalDateTime;

import br.com.mirobook.model.Votes;

public class DetailVotesDto {

	private String nameUser;
	private LocalDateTime dateVotes;

	public DetailVotesDto(Votes votes) {
		this.nameUser = votes.getUser().getName();
		this.dateVotes = votes.getDateVotes();
	}

	public String getNameUser() {
		return nameUser;
	}

	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

	public LocalDateTime getDateVotes() {
		return dateVotes;
	}

	public void setDateVotes(LocalDateTime dateVotes) {
		this.dateVotes = dateVotes;
	}

}
