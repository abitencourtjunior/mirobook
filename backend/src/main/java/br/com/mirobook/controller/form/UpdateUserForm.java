package br.com.mirobook.controller.form;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.mirobook.model.User;
import br.com.mirobook.repository.UserRepository;

public class UpdateUserForm {
	
	@NotNull
	@NotEmpty
	private String name;
	
	@NotNull
	@NotEmpty @Email
	private String mail;
	
	@NotNull
	@NotEmpty
	private String passwordUser;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPasswordUser() {
		return passwordUser;
	}

	public void setPasswordUser(String passwordUser) {
		this.passwordUser = passwordUser;
	}

	public User update(Long id, UserRepository userRepository) {
		User user = userRepository.getOne(id);
		user.setName(name);
		user.setMail(mail);
		user.setPasswordUser(new BCryptPasswordEncoder().encode(passwordUser));
		return user;
	}
	
	

}
