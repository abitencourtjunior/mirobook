package br.com.mirobook.controller.form;

import java.time.LocalDateTime;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.mirobook.model.Post;
import br.com.mirobook.repository.PostRepository;

public class UpdatePostForm {
	
	@NotNull
	@NotEmpty
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Post update(Long id, PostRepository postRepository) {
		Post post = postRepository.getOne(id);
		post.setDescription(description);
		post.setDateCreated(LocalDateTime.now());
		return post;
	}
	
	
	

}
