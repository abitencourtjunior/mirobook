package br.com.mirobook.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.mirobook.controller.dto.DetailsUserDto;
import br.com.mirobook.controller.dto.UserDto;
import br.com.mirobook.controller.form.UpdateUserForm;
import br.com.mirobook.controller.form.UserForm;
import br.com.mirobook.model.User;
import br.com.mirobook.repository.UserRepository;

@Controller
@RequestMapping("/api/user")
public class UserController {
	
	
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping
	public Page<UserDto> listUser(@PageableDefault(page = 0, size = 100, direction = Direction.DESC, sort = "id") Pageable postPagination){
			Page<User> users = userRepository.findAll(postPagination);
			return UserDto.converter(users);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> detailsPost(@PathVariable Long id) {
		Optional<User> optional = userRepository.findById(id);
		if(optional.isPresent()) {
			User user = userRepository.getOne(id);
			return ResponseEntity.ok(new DetailsUserDto(user));
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping
	@Transactional
	public ResponseEntity<UserDto> registerUser(@RequestBody @Valid UserForm form, UriComponentsBuilder uriBuilder){
		User user = form.converter();
		userRepository.save(user);
		
		URI uri = uriBuilder.path("/api/user/{id}").buildAndExpand(user.getId()).toUri();
		return ResponseEntity.created(uri).body(new UserDto(user));
		
	}
	
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<UserDto> update(@PathVariable Long id, @RequestBody @Valid UpdateUserForm updateUserForm) {
		Optional<User> optional = userRepository.findById(id);
		if(optional.isPresent()){
			User user = updateUserForm.update(id, userRepository);
			return ResponseEntity.ok(new UserDto(user));
		}
		
		return ResponseEntity.notFound().build();
	}

	
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<?> delete(@PathVariable Long id){
		Optional<User> optional = userRepository.findById(id);
		if(optional.isPresent()) {
			userRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}
	

}
