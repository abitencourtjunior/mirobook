package br.com.mirobook.controller.form;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.com.mirobook.model.User;

public class UserForm {

	@NotNull
	@NotEmpty
	@Length(min = 5)
	private String name;

	@NotNull
	@NotEmpty
	@Email
	private String email;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	public User converter() {
		return new User(name, email);
	}
}
