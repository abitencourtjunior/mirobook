package br.com.mirobook.controller.dto;

import org.springframework.data.domain.Page;

import br.com.mirobook.model.Post;

public class PostDto {

	private String description;
	private String nameUser;
	private Long upVotes;

	public PostDto(Post post) {
		this.description = post.getDescription();
		this.nameUser = post.getUser().getName();
		this.upVotes = Long.valueOf(post.getListVotes().size());
	}

	public String getDescription() {
		return description;
	}

	public String getNameUser() {
		return nameUser;
	}

	public Long getUpVotes() {
		return upVotes;
	}

	public void setUpVotes(Long upVotes) {
		this.upVotes = upVotes;
	}

	public static Page<PostDto> converter(Page<Post> posts) {
		return posts.map(PostDto::new);
	}

}
