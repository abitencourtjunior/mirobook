package br.com.mirobook.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.mirobook.controller.dto.DetailPostDto;
import br.com.mirobook.controller.dto.PostDto;
import br.com.mirobook.controller.form.PostForm;
import br.com.mirobook.controller.form.UpdatePostForm;
import br.com.mirobook.model.Post;
import br.com.mirobook.repository.PostRepository;
import br.com.mirobook.repository.UserRepository;

@RestController
@RequestMapping("/api/post")
@CrossOrigin(origins = "http://localhost:3000/*")
public class PostController {

	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping
	public Page<PostDto> listPost(@RequestParam(required = false) String description, 
			@PageableDefault(page = 0, size = 100, direction = Direction.DESC, sort = "id") Pageable postPagination){
		if(description == null) {
			Page<Post> posts = postRepository.findAllByVotes(postPagination);
			return PostDto.converter(posts);
		} else {
			Page<Post> posts = postRepository.findByDescription(description, postPagination);
			return PostDto.converter(posts);
		}
	}
	
	@PostMapping
	@Transactional
	public ResponseEntity<PostDto> registerPost(@RequestBody @Valid PostForm form, UriComponentsBuilder uriBuilder){
		Post post = form.converter(userRepository);
		postRepository.save(post);
		
		URI uri = uriBuilder.path("/api/post/{id}").buildAndExpand(post.getId()).toUri();
		return ResponseEntity.created(uri).body(new PostDto(post));
		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> detailsPost(@PathVariable Long id) {
		Optional<Post> optional = postRepository.findById(id);
		if(optional.isPresent()) {
			Post post = postRepository.getOne(id);
			return ResponseEntity.ok(new DetailPostDto(post));
		}
		
		return ResponseEntity.notFound().build();
	}

	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<PostDto> update(@PathVariable Long id, @RequestBody @Valid UpdatePostForm updatePostForm) {
		Optional<Post> optional = postRepository.findById(id);
		if(optional.isPresent()){
			Post post = updatePostForm.update(id, postRepository);
			return ResponseEntity.ok(new PostDto(post));
		}
		
		return ResponseEntity.notFound().build();
	}

	
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<?> delete(@PathVariable Long id){
		Optional<Post> optional = postRepository.findById(id);
		if(optional.isPresent()) {
			postRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}
	
	
	
}
