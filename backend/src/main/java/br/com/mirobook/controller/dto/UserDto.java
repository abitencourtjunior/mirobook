package br.com.mirobook.controller.dto;

import org.springframework.data.domain.Page;

import br.com.mirobook.model.User;

public class UserDto {
	
	private String name;

	public UserDto(User user) {
		this.name = user.getName();
	}

	public String getName() {
		return name;
	}

	public static Page<UserDto> converter(Page<User> users) {
		return users.map(UserDto::new);
	}
	
	
	
}
