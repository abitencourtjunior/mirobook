package br.com.mirobook.controller.dto;

import br.com.mirobook.model.Votes;

public class VotesDto {
	
	private Long id;
	private String description;
	
	public VotesDto(Votes votes) {
		this.id = votes.getId();
		this.description = votes.getPost().getDescription(); 
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
	

	
	

}
