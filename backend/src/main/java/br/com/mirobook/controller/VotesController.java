package br.com.mirobook.controller;

import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.mirobook.controller.dto.VotesDto;
import br.com.mirobook.controller.form.VotesForm;
import br.com.mirobook.model.Post;
import br.com.mirobook.model.User;
import br.com.mirobook.model.Votes;
import br.com.mirobook.repository.PostRepository;
import br.com.mirobook.repository.UserRepository;
import br.com.mirobook.repository.VotesRepository;

@Controller
@RequestMapping("/api/votes")
public class VotesController {
	
	@Autowired
	private VotesRepository votesRepository;
	
	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@PostMapping
	@Transactional
	public ResponseEntity<VotesDto> registerUpVotes(@RequestBody @Valid VotesForm form){
		Optional<User> user = userRepository.findById(form.getIdUser());
		Optional<Post> post = postRepository.findById(form.getIdPost());
		if(user.isPresent() && post.isPresent()) {
			
			Votes votes = new Votes(user.get(), post.get());
			
			votes = votesRepository.findUpVotesByUserPost(votes.getPost().getId(), votes.getUser().getId());
			
			if(votes != null) {
				
				if(votes.getUpVotes()) {
					votes.setUpVotes(false);
					return ResponseEntity.ok(new VotesDto(votes));
				} else {
					votes.setUpVotes(true);
					return ResponseEntity.ok(new VotesDto(votes));
				}
				
			}
			
			votes = new Votes(user.get(), post.get());
			votes.setPost(post.get());
			votes.setUser(user.get());
			
			votesRepository.save(votes);		
			
			return ResponseEntity.ok(new VotesDto(votes));
		}
		
		return ResponseEntity.badRequest().build();
		
		
	}

}
