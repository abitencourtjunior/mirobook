package br.com.mirobook.controller.dto;

import br.com.mirobook.model.User;

public class DetailsUserDto {
	
	private Long id;
	private String name;
	private String mail;
	
	public DetailsUserDto(User user) {
		this.id = user.getId();
		this.name = user.getName();
		this.mail = user.getMail();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
	

}
