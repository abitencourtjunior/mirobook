package br.com.mirobook.config.dto;

public class ErrorFormValidation {
	
	private String mensage;
	private String error;
	
	public ErrorFormValidation(String mensage, String error) {
		this.mensage = mensage;
		this.error = error;
	}

	public String getMensage() {
		return mensage;
	}

	public String getError() {
		return error;
	}



}
