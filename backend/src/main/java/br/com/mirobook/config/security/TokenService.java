package br.com.mirobook.config.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import br.com.mirobook.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenService {

	@Value("${mirobook.jwt.expiration}")
	private String expiration;

	@Value("${mirobook.jwt.secret}")
	private String secret;

	public String generateToken(Authentication authentication) {

		User userLoged = (User) authentication.getPrincipal();
		Date dateGenerated = new Date();

		Date dataExpiration = new Date(dateGenerated.getTime() + Long.parseLong(expiration));

		return Jwts.builder().setIssuer("API - Mirobook").
				setSubject(userLoged.getId().toString())
				.setIssuedAt(dateGenerated).
				setExpiration(dataExpiration).
				signWith(SignatureAlgorithm.HS256, secret)
				.compact();
	}

	public boolean isTokenValid(String token) {
		try {
			Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public Long getIdUser(String token) {
		Claims claims = Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token).getBody();
		return Long.parseLong(claims.getSubject());
	}

}
