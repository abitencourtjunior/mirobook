package br.com.mirobook.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.mirobook.model.Post;

public interface PostRepository extends JpaRepository<Post, Long> {
	
	Page<Post> findByDescription(String description, Pageable postPagination);
	
	@Query("SELECT p FROM Post p join p.listVotes votes where votes.upVotes = 1")
	Page<Post> findAllByVotes(Pageable postPagination);

}
