package br.com.mirobook.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.mirobook.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	
	User findByName(String name);
	
	Optional<User> findByMail(String mail);

}
