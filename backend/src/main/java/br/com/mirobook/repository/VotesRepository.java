package br.com.mirobook.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.mirobook.model.Votes;

public interface VotesRepository extends JpaRepository<Votes, Long> {

	/*
	@Query("SELECT COUNT(*) FROM Votes v FROM v.upVotes = true and v.post.id =: post")
	Long findUpVotes(@Param("post")Long idPost);
	*/
	List<Votes> findByPostId(Long id);
	
	@Query("SELECT COUNT(*) FROM Votes v join v.post post join v.user user where v.upVotes = 1 and post.id =:post")
	Long countUpVotesByUser(@Param("post") Long post);
	
	@Query("SELECT v FROM Votes v join v.post post join v.user user where post.id =:post and user.id =:user")
	Votes findUpVotesByUserPost(@Param("post") Long post, @Param("user") Long user);
}
